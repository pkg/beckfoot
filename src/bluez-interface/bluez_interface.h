/* vim:set expandtab ts=4 shiftwidth=4: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * bluez_interface.h
 *
 *  Created on: Mar 11, 2013
 */

#ifndef BLUEZ_INTERFACE_H_
#define BLUEZ_INTERFACE_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib.h>
#include <ctype.h>
#include <string.h>

void
replace_string (gchar *orig_str, gchar *mod_str);
GList *
filter_for_bluetooth_list (GVariant *network_list);
void
remove_colon_from_string (gchar *orig_str, gchar *mod_str);
gint
list_custome_search (gconstpointer a, gconstpointer b);

typedef enum
{

  BLUEZ_SRV_DEBUG = 1 << 0,

} BluezSrvDebugFlag;
#define BLUEZ_SRV_HAS_DEBUG               ((bluez_service_debug_flags) & 1)
#define BLUEZ_DNLD_PRINT( a ...) \
    if (G_LIKELY (BLUEZ_SRV_HAS_DEBUG )) \
    {			            	\
        g_print("BLUEZService::> " a);		\
    }
extern guint bluez_service_debug_flags;

#endif /* BLUEZ_INTERFACE_H_ */
