/* vim:set expandtab ts=4 shiftwidth=4: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * connman_interface.c
 *
 *  Created on: Apr 8, 2013
 */

#include "../connection_manager.h"
#include "connman_interface.h"
#include "connman_manager_gdbus_fi.h"
#include "connman_technology_gdbus_fi.h"
#include "connman_agent_gdbus_fi.h"
#include "connman_service_gdbus_fi.h"
#include "../bluez-interface/bluez_interface.h"

guint connman_service_debug_flags = 0;

static const GDebugKey connman_debug_keys[] =
  {
    { "connman-mgr", CONNMAN_SRV_DEBUG } };

NetConnmanManager *net_connman_mng_proxy;
NetConnmanTechnology *net_connman_tech_proxy;
GHashTable *ghash_table_passphrase;
GList* service_object_paths;

static struct BeckfootConnmanFunctionInit
{
  connman_connect_notify_callback fn_connman_connect_cb;
  connman_error_notify_callback fn_connman_error_cb;
  connman_disconnect_notify_callback fn_connman_disconnect_cb;
  connman_technology_notify_callback fn_connman_technology_cb;
  beckfoot_connman_status_notify_callback fn_connman_connection_status_cb;
  set_beckfoot_connman_status_properties fn_connman_set_status_property_cb;
} beckfootconnmancallback;

static struct BeckfootWiFiFunctionInit
{
  wifi_connected_network_properties_callback fn_wifi_connected_nw_cb;
  emit_wifi_signal_strength fn_wifi_signal_strength_cb;

} beckfootwificallback;

static struct BeckfootBTFunctionInit
{
  bt_connected_network_properties_callback fn_connected_nw_notify;

} beckfootbtcallback;

static void
glist_custom_free (gpointer data)
{
  if (data)
    {
      g_free (data);
      data = NULL;
    }
}

static gint
list_custome_compare (gconstpointer a, gconstpointer b)
{
  return g_strcmp0 ((gchar *) a, (gchar *) b);
}

static void
ghashtable_free (gpointer pvalue)
{
  if (pvalue)
    {
      g_free (pvalue);
      pvalue = NULL;
    }
}

static void
g_hash_for_passphrase_init (void)
{
  ghash_table_passphrase = g_hash_table_new_full (g_str_hash, g_str_equal,
                                                  ghashtable_free,
                                                  ghashtable_free);
}

static void
net_connman_services_property_changed (NetConnmanService *object,
                                       const gchar *arg_unnamed_arg0,
                                       GVariant *arg_unnamed_arg1)
{
  const gchar *state_is;
  gchar *network_type = NULL;
  guchar strength_is;

  CONNMAN_DNLD_PRINT (" \n\n\n ");
  CONNMAN_DNLD_PRINT (" Start\n");
  CONNMAN_DNLD_PRINT (" Function %s\n", __FUNCTION__);
  CONNMAN_DNLD_PRINT (" State = %s\n", g_variant_print (arg_unnamed_arg1, 1));

  if ((g_strcmp0 (arg_unnamed_arg0, "State") == 0))
    {
      GVariant *temp_variant = g_variant_get_variant (arg_unnamed_arg1);
      state_is = g_variant_get_string (temp_variant, NULL);
      if (g_strrstr (g_dbus_proxy_get_object_path ((GDBusProxy*) object),
                     "wifi"))
        {
          network_type = g_strdup (BECKFOOT_NETWORK_TYPE_WIFI);
        }
      else if (g_strrstr (g_dbus_proxy_get_object_path ((GDBusProxy*) object),
                          "bluetooth"))
        {
          network_type = g_strdup (BECKFOOT_NETWORK_TYPE_BLUETOOTH);
        }
      else if (g_strrstr (g_dbus_proxy_get_object_path ((GDBusProxy*) object),
                          "3g"))
        {

        }

      if ((g_strcmp0 (state_is, "disconnect") == 0)
          || (g_strcmp0 (state_is, "failure") == 0))
        beckfootconnmancallback.fn_connman_disconnect_cb (
            g_dbus_proxy_get_object_path ((GDBusProxy*) object), network_type,
            state_is);
      else
        beckfootconnmancallback.fn_connman_connect_cb (
            g_dbus_proxy_get_object_path ((GDBusProxy*) object), network_type,
            state_is);
    }
  else if ((g_strcmp0 (arg_unnamed_arg0, "Strength") == 0))
    {
      GVariant *temp_variant1 = g_variant_get_variant (arg_unnamed_arg1);
      strength_is = g_variant_get_byte (temp_variant1);
      CONNMAN_DNLD_PRINT(" Signal Strength = %d\n", strength_is);
      beckfootwificallback.fn_wifi_signal_strength_cb (
          g_dbus_proxy_get_object_path ((GDBusProxy*) object), strength_is);
    }

  if (network_type)
    g_free (network_type);
  CONNMAN_DNLD_PRINT(" Finished \n\n\n");
}

static void
net_connman_service_call_connect_clb (GObject *source_object, GAsyncResult *res,
                                      gpointer user_data)
{
  GError *error = NULL;

  CONNMAN_DNLD_PRINT ("\n\n\n Server %s:\n\n\n", __FUNCTION__);

  net_connman_service_call_connect_finish ((NetConnmanService *) source_object,
                                           res, &error);
  if (error)
    {
      const gchar *service_path = user_data;
      gchar *striped_error = NULL;
      gchar *dup_error = g_strdup (error->message);

      striped_error = g_strrstr (dup_error, ":");

      g_warning ("Could not complete the connect service method %s",
                 error ? error->message : "no error given.");

      if (striped_error == NULL)
        {
          beckfootconnmancallback.fn_connman_error_cb (service_path,
                                                       error->message);
        }
      else
        {
          if (g_str_has_prefix (striped_error + 1, " Input"))  // space is required before Input
            {
              // Do nothing
            }
          else
            {
              gchar * ptr = NULL;
              ptr = g_strrstr (striped_error + 1,
                               "\"net.connman.Service\" doesn't exist");
              if (ptr == NULL)
                {
                  beckfootconnmancallback.fn_connman_error_cb (
                      service_path, striped_error + 1);
                  // dont free the "striped_error" as it was freed by freeing "dup_error"
                }
              else
                {
                  beckfootconnmancallback.fn_connman_error_cb (
                      service_path, "Device is not in range");
                  // dont free the "striped_error" as it was freed by freeing "dup_error"
                }

            }
        }
      g_error_free (error);
      error = NULL;
      g_free (dup_error);
      dup_error = NULL;
    }
}

static void
net_connman_service_proxy_created_clb (GObject *source_object,
                                       GAsyncResult *res, gpointer user_data)
{
  GError *error = NULL;
  NetConnmanService *net_connman_service_proxy;

  CONNMAN_DNLD_PRINT ("Server %s:\n", __FUNCTION__);

  /* finishes the proxy creation and gets the proxy ptr */
  net_connman_service_proxy = net_connman_service_proxy_new_finish (res,
                                                                    &error);

  if ((net_connman_service_proxy == NULL) || error)
    {
      g_warning ("Error initializing the net.connman service proxy %s",
                 error ? error->message : "no error given.");
      exit (0);
    }
  if (NULL != net_connman_service_proxy)
    {
      GList *path_list = NULL;
      service_object_paths = g_list_first (service_object_paths);
      path_list = g_list_find_custom (service_object_paths, user_data,
                                      list_custome_compare);
      if (path_list == NULL)
        {
          g_signal_connect (net_connman_service_proxy, "property-changed",
                            G_CALLBACK (net_connman_services_property_changed),
                            NULL);
          service_object_paths = g_list_append (service_object_paths,
                                                g_strdup ((gchar *) user_data));
        }

      net_connman_service_call_connect (net_connman_service_proxy, NULL,
                                        net_connman_service_call_connect_clb,
                                        user_data);
    }
}

static void
net_connman_service_name_appeared (GDBusConnection *connection,
                                   const gchar *name, const gchar *name_owner,
                                   gpointer user_data)

{
  CONNMAN_DNLD_PRINT("net.connman service Name_appeared \n");
  CONNMAN_DNLD_PRINT(" Service object path = %s\n", (gchar * ) user_data);

  net_connman_service_proxy_new (connection, G_DBUS_PROXY_FLAGS_NONE,
                                 "net.connman", (gchar *) user_data, NULL,
                                 net_connman_service_proxy_created_clb,
                                 user_data);
}

static void
net_connman_service_name_vanished (GDBusConnection *connection,
                                   const gchar *name, gpointer user_data)
{
  CONNMAN_DNLD_PRINT("net_connman_service_name_vanished \n");
}

static void
connect_to_service_object_path (const gchar *arg_network_id,
                                const gchar *arg_network_passkey)
{
  gchar *service_object_path = NULL;
  /* Starts watching name on the bus specified by bus_type */
  /* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
  if ((arg_network_id == NULL))
    {
      beckfootconnmancallback.fn_connman_error_cb ("", "Network Id is NULL");
      return;
    }
  service_object_path = g_strdup (arg_network_id);
  g_hash_table_replace (ghash_table_passphrase, g_strdup (arg_network_id),
                        g_strdup (arg_network_passkey));

  g_bus_watch_name (G_BUS_TYPE_SYSTEM, "net.connman",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    net_connman_service_name_appeared,
                    net_connman_service_name_vanished, service_object_path,
                    g_free);
}

static void
set_technology_properties (void)
{
  GVariant *technology_list = NULL;
  GVariant *returned_technology_list = NULL;
  GError *error = NULL;

  if (NULL != net_connman_mng_proxy)
    {
      net_connman_manager_call_get_technologies_sync (net_connman_mng_proxy,
                                                      &returned_technology_list,
                                                      NULL, &error);
      if (error)
        {
          g_warning ("Get Technologies sync failed %s",
                     error ? error->message : "no error given.");
          g_error_free (error);
          error = NULL;
          return;
        }
      else
      CONNMAN_DNLD_PRINT("GetTechnologies success \n");

      technology_list = g_variant_new ("(@a(oa{sv}))",
                                       returned_technology_list);
      filter_for_technologies_set_accordingly (technology_list);
    }
}

void
filter_for_technologies_set_accordingly (GVariant * technology_list)
{

  gboolean wifi_on = FALSE;
  gboolean ethernet_on = FALSE;
  gboolean threeg_on = FALSE;
  gboolean bluetooth_on = FALSE;

  if (technology_list != NULL)
    {
      gchar *name2 = NULL, *field = NULL;
      GVariantIter iter, array_iter;
      GVariant *array = NULL, *array_value = NULL;
      GVariantIter *fields;
      GVariant *value1 = NULL;

      g_variant_iter_init (&iter, technology_list);
      array = g_variant_iter_next_value (&iter);
      g_variant_iter_init (&array_iter, array);
      while ((array_value = g_variant_iter_next_value (&array_iter)) != NULL)
        {
          g_variant_get (array_value, "(oa{sv})", &name2, &fields);
          if (g_strrstr (name2, "wifi"))
            {
              while (g_variant_iter_next (fields, "{sv}", &field, &value1))
                {
                  if ((g_strcmp0 (field, "Powered") == 0))
                    {
                      CONNMAN_DNLD_PRINT("WIFI Powered %s\n",
                                         g_variant_print (value1, 1));
                      if (g_variant_get_boolean (value1) == TRUE)
                        {
                          beckfootconnmancallback.fn_connman_technology_cb (
                              "wifi", TRUE);
                          wifi_on = TRUE;
                        }
                      else
                        {
                          beckfootconnmancallback.fn_connman_technology_cb (
                              "wifi", FALSE);
                          wifi_on = FALSE;
                        }
                    }
                }
            }
          else if (g_strrstr (name2, "ethernet"))
            {
              while (g_variant_iter_next (fields, "{sv}", &field, &value1))
                {
                  if ((g_strcmp0 (field, "Powered") == 0))
                    {
                      CONNMAN_DNLD_PRINT("ethernet Powered %s\n",
                                         g_variant_print (value1, 1));
                      if (g_variant_get_boolean (value1) == TRUE)
                        {
                          beckfootconnmancallback.fn_connman_technology_cb (
                              "ethernet", TRUE);
                          ethernet_on = TRUE;
                        }
                      else
                        {
                          beckfootconnmancallback.fn_connman_technology_cb (
                              "ethernet", FALSE);
                          ethernet_on = FALSE;
                        }
                    }
                }
            }
          else if (g_strrstr (name2, "bluetooth"))
            {
              while (g_variant_iter_next (fields, "{sv}", &field, &value1))
                {
                  if ((g_strcmp0 (field, "Powered") == 0))
                    {
                      CONNMAN_DNLD_PRINT("bluetooth Powered %s\n",
                                         g_variant_print (value1, 1));
                      if (g_variant_get_boolean (value1) == TRUE)
                        {
                          beckfootconnmancallback.fn_connman_technology_cb (
                              "bluetooth", TRUE);
                          bluetooth_on = TRUE;
                        }
                      else
                        {
                          beckfootconnmancallback.fn_connman_technology_cb (
                              "bluetooth", FALSE);
                          bluetooth_on = FALSE;
                        }
                    }
                }
            }
          else if (g_strrstr (name2, "3g"))
            {
              beckfootconnmancallback.fn_connman_technology_cb ("3g", TRUE);
            }
        }
    }

  if (wifi_on == FALSE)
    beckfootconnmancallback.fn_connman_technology_cb ("wifi", FALSE);
  if (threeg_on == FALSE)
    beckfootconnmancallback.fn_connman_technology_cb ("3g", FALSE);
  if (bluetooth_on == FALSE)
    beckfootconnmancallback.fn_connman_technology_cb ("bluetooth", FALSE);
  if (ethernet_on == FALSE)
    beckfootconnmancallback.fn_connman_technology_cb ("ethernet", FALSE);

}

static void
initialise_beckfoot_control_properties (void)
{
  GVariant *network_list = NULL;
  GVariant *returned_service_list = NULL;
  GError *error = NULL;

  if (net_connman_mng_proxy != NULL)
    {

      net_connman_manager_call_get_services_sync (net_connman_mng_proxy,
                                                  &returned_service_list, NULL,
                                                  &error);
      if (error)
        {
          g_warning ("Get Service sync failed %s",
                     error ? error->message : "no error given.");
          g_error_free (error);
          error = NULL;
        }
      else
      CONNMAN_DNLD_PRINT("GetServices success \n");

      network_list = g_variant_new ("(@a(oa{sv}))",
                                    returned_service_list);
      filter_for_connected_nw_info (network_list);
      set_technology_properties ();
    }
}

static void
net_connman_property_changed (NetConnmanManager *object,
                              const gchar *arg_unnamed_arg0,
                              GVariant *arg_unnamed_arg1)
{
  GVariant *temp_variant = NULL;
  const gchar *state_is = NULL;

  CONNMAN_DNLD_PRINT(" \n\n\n");
  CONNMAN_DNLD_PRINT(" Start\n");
  CONNMAN_DNLD_PRINT(" Function %s\n", __FUNCTION__);
  CONNMAN_DNLD_PRINT(" State = %s\n", g_variant_print (arg_unnamed_arg1, 1));

  initialise_beckfoot_control_properties ();

  temp_variant = g_variant_get_variant (arg_unnamed_arg1);
  state_is = g_variant_get_string (temp_variant, NULL);
  CONNMAN_DNLD_PRINT(" State = %s\n", state_is);
#if 0
  // TODo: get connected n/w info from status and emit the signal
  const gchar *network_id = beckfootwificallback.fn_connected_network_notify();
  CONNMAN_DNLD_PRINT(" Connected Network Id  = %s\n", network_id);
  service_object_paths = g_list_first(service_object_paths);
  GList *path_list = NULL;
  path_list = g_list_find_custom(service_object_paths, network_id, list_custome_compare);
  if (path_list == NULL)
    {
      beckfootwificallback.fn_emit_connected_nw_notify(state_is);
    }
#endif

  if (g_strcmp0 (state_is, "online") == 0)
    beckfootconnmancallback.fn_connman_connection_status_cb (TRUE);
  else if (g_strcmp0 (state_is, "idle") == 0)
    beckfootconnmancallback.fn_connman_connection_status_cb (FALSE);
  CONNMAN_DNLD_PRINT(" Finished \n\n\n");
}

static void
net_connman_technology_added (NetConnmanManager *object,
                              const gchar *arg_unnamed_arg0,
                              GVariant *arg_unnamed_arg1)
{
  CONNMAN_DNLD_PRINT(" Function %s\n", __FUNCTION__);
  CONNMAN_DNLD_PRINT(" techAdded = %s\n", g_variant_print (arg_unnamed_arg1, 1));
  CONNMAN_DNLD_PRINT(" Name = %s\n", arg_unnamed_arg0);
  sleep (5);
  initialise_beckfoot_control_properties ();
}

static void
net_connman_technology_removed (NetConnmanManager *object,
                                const gchar *arg_unnamed_arg0)
{
  CONNMAN_DNLD_PRINT(" Function %s\n", __FUNCTION__);
  CONNMAN_DNLD_PRINT(" Technology Removed = %s\n", arg_unnamed_arg0);

  if (g_strrstr (arg_unnamed_arg0, "wifi"))
    {
      beckfootconnmancallback.fn_connman_technology_cb ("wifi", FALSE);
    }
  else if (g_strrstr (arg_unnamed_arg0, "bluetooth"))
    {
      beckfootconnmancallback.fn_connman_technology_cb ("bluetooth", FALSE);
    }
  else if (g_strrstr (arg_unnamed_arg0, "ethernet"))
    {
      beckfootconnmancallback.fn_connman_technology_cb ("ethernet", FALSE);
    }
  else if (g_strrstr (arg_unnamed_arg0, "3g"))
    {
      beckfootconnmancallback.fn_connman_technology_cb ("3g", FALSE);
    }

}

static void
register_for_agent_finish_clb (GObject *source_object, GAsyncResult *res,
                               gpointer user_data)
{
  GError *error = NULL;
  net_connman_manager_call_register_agent_finish (
      (NetConnmanManager *) source_object, res, &error);
  if (error)
    {
      g_warning (" RegisterAgent failed %s",
                 error ? error->message : "no error given.");
      g_error_free (error);
      error = NULL;
    }
  else
  CONNMAN_DNLD_PRINT("RegisterAgent success \n");
}

static void
register_for_agent (void)
{
  net_connman_manager_call_register_agent (net_connman_mng_proxy,
                                           BECKFOOT_CONNMAN_AGENT_PATH, NULL,
                                           register_for_agent_finish_clb, NULL);
}

static void
net_connman_manager_proxy_created_clb (GObject *source_object,
                                       GAsyncResult *res, gpointer user_data)
{
  GError *error = NULL;
  /* finishes the proxy creation and gets the proxy ptr */
  net_connman_mng_proxy = net_connman_manager_proxy_new_finish (res, &error);

  if ((net_connman_mng_proxy == NULL) || error)
    {
      g_warning ("Error intializing the net.connman mng proxy %s",
                 error ? error->message : "no error given.");
      // exit(0);
    }
  else
    {
      register_for_agent ();
      start_net_connman_agent_service ();
      g_signal_connect (net_connman_mng_proxy, "property-changed",
                        G_CALLBACK (net_connman_property_changed), NULL);
      g_signal_connect (net_connman_mng_proxy, "technology-added",
                        G_CALLBACK (net_connman_technology_added), NULL);
      g_signal_connect (net_connman_mng_proxy, "technology-removed",
                        G_CALLBACK (net_connman_technology_removed), NULL);
      initialise_beckfoot_control_properties ();
    }
}

static void
net_connman_technology_proxy_created_clb (GObject *source_object,
                                          GAsyncResult *res, gpointer user_data)
{
  GError *error = NULL;
  /* finishes the proxy creation and gets the proxy ptr */
  net_connman_tech_proxy = net_connman_technology_proxy_new_finish (res,
                                                                    &error);

  if ((net_connman_tech_proxy == NULL) || error)
    {
      g_warning ("Error intializing the net.connman tech proxy %s",
                 error ? error->message : "no error given.");
      exit (0);
    }
}

static void
net_connman_name_appeared (GDBusConnection *connection, const gchar *name,
                           const gchar *name_owner, gpointer user_data)

{
  CONNMAN_DNLD_PRINT("net.connman Name_appeared \n");
  net_connman_manager_proxy_new (connection, G_DBUS_PROXY_FLAGS_NONE,
                                 "net.connman", "/", NULL,
                                 net_connman_manager_proxy_created_clb,
                                 user_data);
  net_connman_technology_proxy_new (connection, G_DBUS_PROXY_FLAGS_NONE,
                                    "net.connman",
                                    "/net/connman/technology/wifi", NULL,
                                    net_connman_technology_proxy_created_clb,
                                    user_data);
}

static void
net_connman_name_vanished (GDBusConnection *connection, const gchar *name,
                           gpointer user_data)
{
  CONNMAN_DNLD_PRINT("net_connman_name_vanished \n");

  if (NULL != net_connman_mng_proxy)
    {
      g_object_unref (net_connman_mng_proxy);
    }
  else if (NULL != net_connman_tech_proxy)
    g_object_unref (net_connman_tech_proxy);

  g_hash_table_destroy (ghash_table_passphrase);
  service_object_paths = g_list_first (service_object_paths);
  g_list_free_full (service_object_paths, glist_custom_free);
}

void
initialise_net_connman_gdbus_proxy (void)
{
  const char *env_string;
  env_string = g_getenv ("CONNMAN_SRV_DEBUG");
  if (env_string != NULL)
    {
      connman_service_debug_flags = g_parse_debug_string (
          env_string, connman_debug_keys, G_N_ELEMENTS (connman_debug_keys));
    }

  g_hash_for_passphrase_init ();
  g_bus_watch_name (G_BUS_TYPE_SYSTEM, "net.connman",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    net_connman_name_appeared, net_connman_name_vanished, NULL,
                    NULL);
}

static GVariant *
filter_for_wifi_list (GVariant *network_list)
{
  if (network_list != NULL)
    {
      GVariantBuilder *builder1, *builder2;
      GVariant *value_builder1 = NULL;
      gchar *name2 = NULL, *field = NULL;
      GVariantIter iter, array_iter;
      GVariant *array = NULL, *array_value = NULL;
      GVariantIter *fields;
      GVariant *value1 = NULL;

      builder2 = g_variant_builder_new (G_VARIANT_TYPE ("a(oa{sv})"));
      g_variant_iter_init (&iter, network_list);
      array = g_variant_iter_next_value (&iter);
      g_variant_iter_init (&array_iter, array);
      while ((array_value = g_variant_iter_next_value (&array_iter)) != NULL)
        {
          g_variant_get (array_value, "(oa{sv})", &name2, &fields);
          if (g_strrstr (name2, "wifi"))
            {
              CONNMAN_DNLD_PRINT(" Object Path %s\n", name2);
              builder1 = g_variant_builder_new (G_VARIANT_TYPE ("a{sv}"));

              while (g_variant_iter_next (fields, "{sv}", &field, &value1))
                {

                  if ((g_strcmp0 (field, "Type") == 0)
                      || (g_strcmp0 (field, "Security") == 0)
                      || (g_strcmp0 (field, "Strength") == 0)
                      || (g_strcmp0 (field, "Name") == 0)
                      || (g_strcmp0 (field, "State") == 0)
                      || (g_strcmp0 (field, "Ethernet") == 0)
                      || (g_strcmp0 (field, "AutoConnect") == 0))
                    {  // AutoConnec

                      if (g_strcmp0 (field, "Ethernet") == 0)
                        {
                          GVariantIter *ethernet;
                          gchar *mac_id;
                          GVariant *mac_value;

                          CONNMAN_DNLD_PRINT ("%s:%s\n", field,
                                              g_variant_print (value1, 2));

                          g_variant_get (value1, "a{sv}", &ethernet);
                          while (g_variant_iter_next (ethernet, "{sv}", &mac_id,
                                                      &mac_value))
                            {
                              CONNMAN_DNLD_PRINT("Ethernet --> %s:%s\n", mac_id,
                                                 g_variant_print (mac_value, 1));
                              if (g_strcmp0 (mac_id, "Address") == 0)
                                g_variant_builder_add (builder1, "{sv}",
                                                       "MAC-Address",
                                                       mac_value);
                            }
                        }
                      else
                        {
                          CONNMAN_DNLD_PRINT("%s:%s\n", field,
                                             g_variant_print (value1, 2));
                          g_variant_builder_add (builder1, "{sv}", field,
                                                 value1);
                        }
                    }
                }

              value_builder1 = g_variant_new ("(oa{sv})", name2, builder1);
              g_variant_builder_add_value (builder2, value_builder1);
            }
        }

      value_builder1 = g_variant_new ("(@a(oa{sv}))",
                                      g_variant_builder_end (builder2));
      return value_builder1;
    }
  return NULL;

}

GVariant*
get_network_list (void)
{
  GVariant *returned_service_list = NULL;
  GVariant *filtered_wifi_list = NULL;
  GError *error = NULL;

  net_connman_technology_call_scan_sync (net_connman_tech_proxy, NULL, &error);
  if (error)
    {
      gchar *striped_error = NULL;
      gchar *dup_error = g_strdup (error->message);

      g_warning ("Could not trigger wifi scan %s",
                 error ? error->message : "no error given.");

      striped_error = g_strrstr (dup_error, ":");
      if (striped_error == NULL)
        beckfootconnmancallback.fn_connman_error_cb ("net.connman.Technology",
                                                     error->message);
      else
        {
          gchar * ptr = NULL;
          ptr = g_strrstr (striped_error + 1,
                           "\"net.connman.Technology\" doesn't exist");

          if (ptr == NULL)
            {
              if (g_strcmp0 (" Not supported", striped_error + 1) == 0)
                beckfootconnmancallback.fn_connman_error_cb (
                    "net.connman.Technology", "WiFi is OFF");
              else
                beckfootconnmancallback.fn_connman_error_cb (
                    "net.connman.Technology", striped_error + 1);
              // dont free the "striped_error" as it was freed by freeing "dup_error"
            }
          else
            {
              beckfootconnmancallback.fn_connman_error_cb (
                  "net.connman.Technology", "WiFi is OFF");
              // dont free the "striped_error" as it was freed by freeing "dup_error"
            }
        }

      g_error_free (error);
      error = NULL;
      g_free (dup_error);
      dup_error = NULL;

    }
  else
    {
      CONNMAN_DNLD_PRINT("wifi Scan is success \n");

      net_connman_manager_call_get_services_sync (net_connman_mng_proxy,
                                                  &returned_service_list, NULL,
                                                  &error);
      if (error)
        {
          g_warning ("Get Service sync failed %s",
                     error ? error->message : "no error given.");
          g_error_free (error);
          error = NULL;
          beckfootconnmancallback.fn_connman_error_cb ("net.connman.Technology",
                                                       "Wifi not working");
        }
      else
        {
          CONNMAN_DNLD_PRINT("GetServices success=%s\n",
                             g_variant_print (returned_service_list, 1));

          if (returned_service_list != NULL)
            {
              GVariant *network_list = g_variant_new ("(@a(oa{sv}))",
                                                      returned_service_list);
              filtered_wifi_list = filter_for_wifi_list (network_list);
            }
        }
    }

  return filtered_wifi_list;
}

void
beckfoot_connection_manager_callback (
    connman_connect_notify_callback fn_connman_connect_cb,
    connman_error_notify_callback fn_connman_error_cb,
    connman_disconnect_notify_callback fn_connman_disconnect_cb,
    connman_technology_notify_callback fn_connman_technology_cb,
    beckfoot_connman_status_notify_callback fn_connman_connection_status_cb,
    set_beckfoot_connman_status_properties fn_connman_set_status_property_cb)
{
  beckfootconnmancallback.fn_connman_connect_cb = fn_connman_connect_cb;
  beckfootconnmancallback.fn_connman_error_cb = fn_connman_error_cb;
  beckfootconnmancallback.fn_connman_disconnect_cb = fn_connman_disconnect_cb;
  beckfootconnmancallback.fn_connman_technology_cb = fn_connman_technology_cb;
  beckfootconnmancallback.fn_connman_connection_status_cb =
      fn_connman_connection_status_cb;
  beckfootconnmancallback.fn_connman_set_status_property_cb =
      fn_connman_set_status_property_cb;
}

void
beckfoot_wifi_manager_callback (
    wifi_connected_network_properties_callback fn_wifi_connected_nw_cb,
    emit_wifi_signal_strength fn_wifi_signal_strength_cb)
{
  beckfootwificallback.fn_wifi_connected_nw_cb = fn_wifi_connected_nw_cb;
  beckfootwificallback.fn_wifi_signal_strength_cb = fn_wifi_signal_strength_cb;
}

void
beckfoot_bluetooth_manager_properties_callback (
    bt_connected_network_properties_callback fn_connected_nw_notify)
{
  beckfootbtcallback.fn_connected_nw_notify = fn_connected_nw_notify;
}

void
filter_for_connected_nw_info (GVariant *network_list)
{
  CONNMAN_DNLD_PRINT(" Function %s\n", __FUNCTION__);

  beckfootwificallback.fn_wifi_connected_nw_cb ("", "ObjectPath", 0);
  beckfootwificallback.fn_wifi_connected_nw_cb (BECKFOOT_NETWORK_STATE_OFFLINE,
                                                "Status", 0);
  beckfootwificallback.fn_wifi_connected_nw_cb ("", "SignalStrength", 0);

  beckfootbtcallback.fn_connected_nw_notify ("", "ObjectPath");
  beckfootbtcallback.fn_connected_nw_notify (BECKFOOT_NETWORK_STATE_OFFLINE,
                                             "Status");
  beckfootconnmancallback.fn_connman_set_status_property_cb (
      BECKFOOT_NETWORK_NAME_UNKNOWN, BECKFOOT_NETWORK_STATE_UNKNOWN,
      BECKFOOT_NETWORK_TYPE_UNKNOWN);
  //beckfootconnmancallback.fn_connman_connection_status_cb(FALSE);

  if (network_list != NULL)
    {
      gchar *name2 = NULL, *field = NULL;
      GVariantIter iter, array_iter;
      GVariant *array = NULL, *array_value = NULL;
      GVariantIter *fields;
      GVariant *value1 = NULL;

      g_variant_iter_init (&iter, network_list);
      array = g_variant_iter_next_value (&iter);
      g_variant_iter_init (&array_iter, array);
      while ((array_value = g_variant_iter_next_value (&array_iter)) != NULL)
        {
          g_variant_get (array_value, "(oa{sv})", &name2, &fields);
          if (g_strrstr (name2, "wifi"))
            {

              while (g_variant_iter_next (fields, "{sv}", &field, &value1))
                {
                  if ((g_strcmp0 (field, "State") == 0))
                    {
                      CONNMAN_DNLD_PRINT("State is ready or Online %s \n",
                                         g_variant_print (value1, 1));

                      if ((g_strcmp0 (g_variant_get_string (value1, NULL),
                                      "ready") == 0))
                        {
                          const char *env_string;
                          beckfootwificallback.fn_wifi_connected_nw_cb (
                              name2, "ObjectPath", 0);
                          beckfootwificallback.fn_wifi_connected_nw_cb (
                              "ready", "Status", 0);
                          env_string = g_getenv ("CONNMAN_READY_STATE");
                          if (env_string != NULL)
                            {
                              beckfootconnmancallback.fn_connman_set_status_property_cb (
                                  name2, BECKFOOT_NETWORK_STATE_ONLINE,
                                  BECKFOOT_NETWORK_TYPE_WIFI);
                              beckfootconnmancallback.fn_connman_connection_status_cb (
                                  TRUE);
                            }

                          create_proxy_for_connected_service (name2);  // to catch disconnect signal
                        }
                      else if ((g_strcmp0 (g_variant_get_string (value1, NULL),
                                           "online") == 0))
                        {
                          CONNMAN_DNLD_PRINT(
                              "Object path of the online status connection  %s:\n",
                              name2);

                          beckfootwificallback.fn_wifi_connected_nw_cb (
                              name2, "ObjectPath", 0);
                          beckfootwificallback.fn_wifi_connected_nw_cb (
                              BECKFOOT_NETWORK_STATE_ONLINE, "Status", 0);
                          beckfootconnmancallback.fn_connman_set_status_property_cb (
                              name2, BECKFOOT_NETWORK_STATE_ONLINE,
                              BECKFOOT_NETWORK_TYPE_WIFI);
                          create_proxy_for_connected_service (name2);  // to catch disconnect signal
                          // beckfootconnmancallback.fn_connman_connection_status_cb(TRUE);
                        }
                    }
                  else if ((g_strcmp0 (field, "Strength") == 0))
                    {
                      guchar strength_is = g_variant_get_byte (value1);

                      CONNMAN_DNLD_PRINT (" value1 is  %s\n",
                                          g_variant_print (value1, 1));
                      beckfootwificallback.fn_wifi_connected_nw_cb (
                          name2, "SignalStrength", strength_is);
                    }
                }

            }
          else if (g_strrstr (name2, "bluetooth"))
            {
              while (g_variant_iter_next (fields, "{sv}", &field, &value1))
                {
                  if ((g_strcmp0 (field, "State") == 0))
                    {
                      if ((g_strcmp0 (g_variant_get_string (value1, NULL),
                                      "ready") == 0))
                        {
                          const char *env_string;
                          beckfootbtcallback.fn_connected_nw_notify (
                              name2, "ObjectPath");
                          beckfootbtcallback.fn_connected_nw_notify ("ready",
                                                                     "Status");
                          env_string = g_getenv ("CONNMAN_READY_STATE");
                          if (env_string != NULL)
                            {
                              beckfootconnmancallback.fn_connman_set_status_property_cb (
                                  name2, BECKFOOT_NETWORK_STATE_ONLINE,
                                  BECKFOOT_NETWORK_TYPE_BLUETOOTH);
                              beckfootconnmancallback.fn_connman_connection_status_cb (
                                  TRUE);
                            }
                          create_proxy_for_connected_service (name2);  // to catch disconnect signal

                        }
                      else if ((g_strcmp0 (g_variant_get_string (value1, NULL),
                                           "online") == 0))
                        {
                          beckfootbtcallback.fn_connected_nw_notify (
                              name2, "ObjectPath");
                          beckfootbtcallback.fn_connected_nw_notify (
                              BECKFOOT_NETWORK_STATE_ONLINE, "Status");
                          beckfootconnmancallback.fn_connman_set_status_property_cb (
                              name2, BECKFOOT_NETWORK_STATE_ONLINE,
                              BECKFOOT_NETWORK_TYPE_BLUETOOTH);
                          create_proxy_for_connected_service (name2);  // to catch disconnect signal
                          // beckfootconnmancallback.fn_connman_connection_status_cb(TRUE);
                          CONNMAN_DNLD_PRINT(
                              "Object path of the online status connection  %s:\n",
                              name2);
                        }

                    }

                }

            }
          else if (g_strrstr (name2, "3g"))
            {
              while (g_variant_iter_next (fields, "{sv}", &field, &value1))
                {
                  if ((g_strcmp0 (field, "State") == 0))
                    {
                      if ((g_strcmp0 (g_variant_get_string (value1, NULL),
                                      "ready") == 0))
                        {

                        }
                      else if ((g_strcmp0 (g_variant_get_string (value1, NULL),
                                           "online") == 0))
                        {

                        }

                    }

                }

            }
          else if (g_strrstr (name2, "ethernet"))
            {

              while (g_variant_iter_next (fields, "{sv}", &field, &value1))
                {
                  if ((g_strcmp0 (field, "State") == 0))
                    {
                      if ((g_strcmp0 (g_variant_get_string (value1, NULL),
                                      "ready") == 0))
                        {
                          // Todo: Create connected n/w properties for ethernet and set  it
                          const char *env_string;
                          env_string = g_getenv ("CONNMAN_READY_STATE");
                          if (env_string != NULL)
                            {
                              beckfootconnmancallback.fn_connman_set_status_property_cb (
                                  name2, BECKFOOT_NETWORK_STATE_ONLINE,
                                  BECKFOOT_NETWORK_TYPE_LAN);
                              beckfootconnmancallback.fn_connman_connection_status_cb (
                                  TRUE);
                            }
                          create_proxy_for_connected_service (name2);  // to catch disconnect signal
                        }
                      else if ((g_strcmp0 (g_variant_get_string (value1, NULL),
                                           "online") == 0))
                        {
                          // Todo: Create connected n/w properties for ethernet and set it
                          beckfootconnmancallback.fn_connman_set_status_property_cb (
                              name2, BECKFOOT_NETWORK_STATE_ONLINE,
                              BECKFOOT_NETWORK_TYPE_LAN);
                          //beckfootconnmancallback.fn_connman_connection_status_cb(TRUE);
                          CONNMAN_DNLD_PRINT(
                              "Object path of the online status connection  %s:\n",
                              name2);
                        }

                    }

                }
            }
        }
    }
  else
    {
      CONNMAN_DNLD_PRINT("\n\n\n No Active device \n\n\n");
    }

}

static void
net_connman_service_call_disconnect_clb (GObject *source_object,
                                         GAsyncResult *res, gpointer user_data)
{
  GError *error = NULL;

  CONNMAN_DNLD_PRINT ("\n\n\n Server %s:\n\n\n", __FUNCTION__);
  net_connman_service_call_disconnect_finish (
      (NetConnmanService *) source_object, res, &error);
  if (error)
    {
      const gchar *service_path = user_data;
      gchar *striped_error = NULL;
      gchar *dup_error = g_strdup (error->message);

      g_warning ("Could not complete the disconnect service method %s",
                 error ? error->message : "no error given.");
      striped_error = g_strrstr (dup_error, ":");
      if (striped_error == NULL)
        beckfootconnmancallback.fn_connman_error_cb (service_path,
                                                     error->message);
      else
        {
          beckfootconnmancallback.fn_connman_error_cb (service_path,
                                                       striped_error + 1);
          // dont free the "striped_error" as it was freed by freeing "dup_error"
        }
      g_error_free (error);
      error = NULL;
      g_free (dup_error);
      dup_error = NULL;
    }
}

static void
net_connman_service_disconnect_proxy_created_clb (GObject *source_object,
                                                  GAsyncResult *res,
                                                  gpointer user_data)
{
  GError *error = NULL;
  NetConnmanService *net_connman_service_disconnect_proxy;

  CONNMAN_DNLD_PRINT ("\n\n\n");
  CONNMAN_DNLD_PRINT (" Server %s:", __FUNCTION__);

  /* finishes the proxy creation and gets the proxy ptr */
  net_connman_service_disconnect_proxy = net_connman_service_proxy_new_finish (
      res, &error);

  if ((net_connman_service_disconnect_proxy == NULL) || error)
    {
      g_warning ("Error intializing the net.connman service proxy %s",
                 error ? error->message : "no error given.");
    }
  if (NULL != net_connman_service_disconnect_proxy)
    {
      GList *path_list = NULL;
      service_object_paths = g_list_first (service_object_paths);
      path_list = g_list_find_custom (service_object_paths, user_data,
                                      list_custome_compare);
      if (path_list == NULL)
        {
          g_signal_connect (net_connman_service_disconnect_proxy,
                            "property-changed",
                            G_CALLBACK (net_connman_services_property_changed),
                            NULL);
          service_object_paths = g_list_append (service_object_paths,
                                                g_strdup ((gchar *) user_data));
        }
      net_connman_service_call_disconnect (
          net_connman_service_disconnect_proxy, NULL,
          net_connman_service_call_disconnect_clb, user_data);
    }
  CONNMAN_DNLD_PRINT("\n\n\n");
}

static void
net_connman_service_disconnect_name_appeared (GDBusConnection *connection,
                                              const gchar *name,
                                              const gchar *name_owner,
                                              gpointer user_data)
{
  CONNMAN_DNLD_PRINT("net.connman service disconnect Name_appeared \n");
  CONNMAN_DNLD_PRINT(" Service object path = %s\n", (gchar * ) user_data);
  net_connman_service_proxy_new (
      connection, G_DBUS_PROXY_FLAGS_NONE, "net.connman", (gchar *) user_data,
      NULL, net_connman_service_disconnect_proxy_created_clb, user_data);

}

static void
net_connman_service_disconnect_name_vanished (GDBusConnection *connection,
                                              const gchar *name,
                                              gpointer user_data)
{
  CONNMAN_DNLD_PRINT("net.connman service disconnect name_vanished \n");

  //if (NULL != net_connman_service_proxy) {
  //      g_object_unref(net_connman_service_proxy);
//      }
}

static void
disconnect_to_service_object_path (const gchar *arg_network_id)
{
  gchar *service_object_path = g_strdup (arg_network_id);
  g_bus_watch_name (G_BUS_TYPE_SYSTEM, "net.connman",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    net_connman_service_disconnect_name_appeared,
                    net_connman_service_disconnect_name_vanished,
                    service_object_path, g_free);
}

static void
net_connman_technology_call_set_property_clb (GObject *source_object,
                                              GAsyncResult *res,
                                              gpointer user_data)
{
  GError *error = NULL;

  CONNMAN_DNLD_PRINT ("Server %s:\n", __FUNCTION__);
  net_connman_technology_call_set_property_finish (
      (NetConnmanTechnology *) source_object, res, &error);
  if (error)
    {
      const gchar *tech_type = user_data;
      gchar *striped_error = NULL;
      gchar *dup_error = g_strdup (error->message);

      g_warning ("Could not complete the technology powered method %s",
                 error ? error->message : "no error given.");
      striped_error = g_strrstr (dup_error, ":");
      if (striped_error == NULL)
        beckfootconnmancallback.fn_connman_error_cb (tech_type, error->message);
      else
        beckfootconnmancallback.fn_connman_error_cb (tech_type,
                                                     striped_error + 1);  // dont free the "striped_error" as it was freed by freeing "dup_error"

      g_error_free (error);
      error = NULL;
      g_free (dup_error);
      dup_error = NULL;
    }
  else
    {
      TechnologyInfo *technology_info = (TechnologyInfo *) user_data;

      if ((g_strcmp0 (technology_info->type, "wifi") == 0))
        {
          if (technology_info->powered == 1)
            {
              beckfootconnmancallback.fn_connman_technology_cb ("wifi", TRUE);
            }
          else
            {
              beckfootconnmancallback.fn_connman_technology_cb ("wifi", FALSE);
            }
        }
      else if ((g_strcmp0 (technology_info->type, "ethernet") == 0))
        {
          if (technology_info->powered == 1)
            {
              beckfootconnmancallback.fn_connman_technology_cb ("ethernet",
                                                                TRUE);
            }
          else
            {
              beckfootconnmancallback.fn_connman_technology_cb ("ethernet",
                                                                FALSE);
            }
        }
      else if ((g_strcmp0 (technology_info->type, "bluetooth") == 0))
        {
          if (technology_info->powered == 1)
            {
              beckfootconnmancallback.fn_connman_technology_cb ("bluetooth",
                                                                TRUE);
            }
          else
            {
              beckfootconnmancallback.fn_connman_technology_cb ("bluetooth",
                                                                FALSE);
            }
        }
    }

}

static void
net_connman_technology_powered_proxy_created_clb (GObject *source_object,
                                                  GAsyncResult *res,
                                                  gpointer user_data)
{
  GError *error = NULL;
  gboolean gvalue = FALSE;
  gchar *property_name = g_strdup ("Powered");
  NetConnmanTechnology *net_connman_technology_powered_proxy;

  CONNMAN_DNLD_PRINT ("Server %s:\n", __FUNCTION__);

  /* finishes the proxy creation and gets the proxy ptr */
  net_connman_technology_powered_proxy =
      net_connman_technology_proxy_new_finish (res, &error);

  if ((net_connman_technology_powered_proxy == NULL) || error)
    {
      g_warning (
          "Error initializing the net.connman technology powered proxy %s",
          error ? error->message : "no error given.");
    }

#if 0
  if (NULL != net_connman_technology_powered_proxy)
    {

      TechnologyInfo *technology_info = (TechnologyInfo *) user_data;

      if ((g_strcmp0(technology_info->type, "wifi") == 0))
        {
          if (technology_info->powered == 1)
            {
              gvalue = TRUE;
              beckfootconnmancallback.fn_connman_technology_cb("wifi", TRUE);
            }
          else
            {
              beckfootconnmancallback.fn_connman_technology_cb("wifi", FALSE);
            }
        }

      else if ((g_strcmp0(technology_info->type, "ethernet") == 0))
        {
          if (technology_info->powered == 1)
            {
              gvalue = TRUE;
              beckfootconnmancallback.fn_connman_technology_cb("ethernet", TRUE);
            }
          else
            {
              beckfootconnmancallback.fn_connman_technology_cb("ethernet", FALSE);
            }
        }
    }
#endif

  if (NULL != net_connman_technology_powered_proxy)
    {
      GVariant *flag = NULL;
      TechnologyInfo *technology_info = (TechnologyInfo *) user_data;
      gvalue = technology_info->powered;
      flag = g_variant_new ("v", g_variant_new_boolean (gvalue));
      net_connman_technology_call_set_property (
          net_connman_technology_powered_proxy, property_name, flag, NULL,
          net_connman_technology_call_set_property_clb, user_data);
    }
  g_free (property_name);
}

static void
net_connman_technology_powered_name_appeared (GDBusConnection *connection,
                                              const gchar *name,
                                              const gchar *name_owner,
                                              gpointer user_data)
{
  TechnologyInfo *technology_info = user_data;

  CONNMAN_DNLD_PRINT("net.connman technology powered Name_appeared \n");

  if (g_strcmp0 (technology_info->type, "wifi") == 0)
    {
      net_connman_technology_proxy_new (
          connection, G_DBUS_PROXY_FLAGS_NONE, "net.connman",
          NET_CONNMAN_WIFI_TECHNOLOGY_PATH,
          NULL, net_connman_technology_powered_proxy_created_clb, user_data);
    }
  else if (g_strcmp0 (technology_info->type, "ethernet") == 0)
    {
      net_connman_technology_proxy_new (
          connection, G_DBUS_PROXY_FLAGS_NONE, "net.connman",
          NET_CONNMAN_ETHERNET_TECHNOLOGY_PATH,
          NULL, net_connman_technology_powered_proxy_created_clb, user_data);
    }
  else if (g_strcmp0 (technology_info->type, "bluetooth") == 0)
    {

      net_connman_technology_proxy_new (
          connection, G_DBUS_PROXY_FLAGS_NONE, "net.connman",
          NET_CONNMAN_BLUETOOTH_TECHNOLOGY_PATH,
          NULL, net_connman_technology_powered_proxy_created_clb, user_data);

    }

}

static void
net_connman_technology_powered_name_vanished (GDBusConnection *connection,
                                              const gchar *name,
                                              gpointer user_data)
{
  CONNMAN_DNLD_PRINT("net.connman technology off name_vanished \n");
}

static void
technology_g_free (gpointer data)
{
  TechnologyInfo *technology_info = (TechnologyInfo *) data;
  if (technology_info != NULL)
    if (technology_info->type != NULL)
      {
        g_free (technology_info->type);
        g_free (technology_info);
        technology_info->type = NULL;
        technology_info = NULL;
      }
}

void
technology_powered (const gchar *arg_technology_type, gboolean arg_powered)
{
  TechnologyInfo *technology_info = g_new0 (TechnologyInfo, 1);
  technology_info->type = g_strdup (arg_technology_type);
  technology_info->powered = arg_powered;
  g_bus_watch_name (G_BUS_TYPE_SYSTEM, "net.connman",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    net_connman_technology_powered_name_appeared,
                    net_connman_technology_powered_name_vanished,
                    technology_info, technology_g_free);
}

static void
request_input (NetConnmanAgent *object, GDBusMethodInvocation *invocation,
               const gchar *arg_path, GVariant *arg_fields)
{
  gchar *passphrase = NULL;
  GVariantBuilder *gvb;

  CONNMAN_DNLD_PRINT (" arg_path %s in function %s\n", arg_path, __FUNCTION__);

  passphrase = (gchar*) g_hash_table_lookup (ghash_table_passphrase, arg_path);
  CONNMAN_DNLD_PRINT("passphrase is %s\n", passphrase);
  gvb = g_variant_builder_new (G_VARIANT_TYPE ("a{sv}"));
  g_variant_builder_add (gvb, "{sv}", "Passphrase",
                         g_variant_new_string (passphrase));

  net_connman_agent_complete_request_input (object, invocation,
                                            g_variant_new ("a{sv}", gvb));
}

static void
report_error (NetConnmanAgent *object, GDBusMethodInvocation *invocation,
              const gchar *arg_path, const gchar *arg_error)
{
  gchar *dup_error = NULL;

  CONNMAN_DNLD_PRINT ("Agent error in net.connman.Agent bus and error = %s\n",
                      arg_error);

  if (g_strcmp0 (arg_error, "invalid-key") == 0)
    {
      dup_error = g_strdup ("Incorrect Password");
      beckfootconnmancallback.fn_connman_error_cb (arg_path, dup_error);
      g_free (dup_error);
      dup_error = NULL;
    }
  else
    {
      beckfootconnmancallback.fn_connman_error_cb (arg_path, arg_error);
    }

  net_connman_agent_complete_report_error (object, invocation);
}

static void
on_net_connman_agent_bus_acquired (GDBusConnection *connection,
                                   const gchar *name, gpointer user_data)
{
  NetConnmanAgent *net_connman_agent_proxy = NULL;

  CONNMAN_DNLD_PRINT ("Server %s:\n", __FUNCTION__);
  net_connman_agent_proxy = net_connman_agent_skeleton_new ();
  if (NULL != net_connman_agent_proxy)
    {
      CONNMAN_DNLD_PRINT("Server %s:\n",
                         "net_connman_agent_proxy created successfully");
      g_signal_connect (net_connman_agent_proxy, "handle_request_input",
                        G_CALLBACK (request_input), NULL);
      g_signal_connect (net_connman_agent_proxy, "handle_report_error",
                        G_CALLBACK (report_error), NULL);

      /* Exports interface connectionMgrControl at object_path "/Beckfoot/ConnectionManager/Control" on connection */
      if (!g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (net_connman_agent_proxy), connection,
          BECKFOOT_CONNMAN_AGENT_PATH, NULL))
        CONNMAN_DNLD_PRINT("%s: net.connman.Agent interface export error\n",
                           __FILE__);
    }
}

static void
on_net_connman_agent_name_acquired (GDBusConnection *connection,
                                    const gchar *name, gpointer user_data)
{
  CONNMAN_DNLD_PRINT("Server %s:\n", __FUNCTION__);
  /* Nothing to do */
}

static void
on_net_connman_agent_name_lost (GDBusConnection *connection, const gchar *name,
                                gpointer user_data)
{
  CONNMAN_DNLD_PRINT("Server %s:\n", __FUNCTION__);
  /* free all dbus handlers ... */
}

void
start_net_connman_agent_service (void)
{
  CONNMAN_DNLD_PRINT("Server %s:\n", __FUNCTION__);

  g_bus_own_name (G_BUS_TYPE_SYSTEM,  // bus type
      "net.connman.Agent",  // interface name/ bus or service name
      G_BUS_NAME_OWNER_FLAGS_NONE,  // bus own flag, can be used to take away the bus and give it to another service
      on_net_connman_agent_bus_acquired,  // callback invoked when the bus is acquired
      on_net_connman_agent_name_acquired,  // callback invoked when interface name is acquired
      on_net_connman_agent_name_lost,  // callback invoked when name is lost to another service or other reason
      NULL,  // user data
      NULL);  // user data free func

}

static void
net_connman_connected_service_proxy_created_clb (GObject *source_object,
                                                 GAsyncResult *res,
                                                 gpointer user_data)
{
  GError *error = NULL;
  NetConnmanService *net_connman_connected_service_proxy;

  CONNMAN_DNLD_PRINT ("Server %s:\n", __FUNCTION__);
  /* finishes the proxy creation and gets the proxy ptr */
  net_connman_connected_service_proxy = net_connman_service_proxy_new_finish (
      res, &error);

  if ((net_connman_connected_service_proxy == NULL) || error)
    {
      g_warning ("Error intializing the net.connman connected service proxy %s",
                 error ? error->message : "no error given.");
    }
  if (NULL != net_connman_connected_service_proxy)
    {
      GList *path_list = NULL;
      service_object_paths = g_list_first (service_object_paths);
      path_list = g_list_find_custom (service_object_paths, user_data,
                                      list_custome_compare);
      if (path_list == NULL)
        {
          g_signal_connect (net_connman_connected_service_proxy,
                            "property-changed",
                            G_CALLBACK (net_connman_services_property_changed),
                            NULL);
          service_object_paths = g_list_append (service_object_paths,
                                                g_strdup ((gchar *) user_data));
        }
    }

}

static void
net_connman_connected_service_name_appeared (GDBusConnection *connection,
                                             const gchar *name,
                                             const gchar *name_owner,
                                             gpointer user_data)
{
  CONNMAN_DNLD_PRINT("net.connman connected service Name_appeared \n");
  CONNMAN_DNLD_PRINT(" Service object path = %s\n", (gchar * )user_data);
  net_connman_service_proxy_new (
      connection, G_DBUS_PROXY_FLAGS_NONE, "net.connman", (gchar *) user_data,
      NULL, net_connman_connected_service_proxy_created_clb, user_data);
}

static void
net_connman_connected_service_name_vanished (GDBusConnection *connection,
                                             const gchar *name,
                                             gpointer user_data)
{
  CONNMAN_DNLD_PRINT("net.connman connected service name_vanished \n");
}

void
create_proxy_for_connected_service (gchar *service_path)
{
  gchar *dup_service_path = NULL;

  if (service_path == NULL)
    return;
  dup_service_path = g_strdup (service_path);
  g_bus_watch_name (G_BUS_TYPE_SYSTEM, "net.connman",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    net_connman_connected_service_name_appeared,
                    net_connman_connected_service_name_vanished,
                    dup_service_path, g_free);
}

GVariant *
net_connection_manager_get_service_method (void)
{
  GVariant *returned_service_list = NULL;
  GError *error = NULL;
  GVariant *network_list = NULL;
  net_connman_manager_call_get_services_sync (net_connman_mng_proxy,
                                              &returned_service_list, NULL,
                                              &error);
  if (error)
    {
      g_warning ("Get Service sync failed %s",
                 error ? error->message : "no error given.");
      g_error_free (error);
      error = NULL;
    }
  else
    {
      CONNMAN_DNLD_PRINT("GetServices success \n");
      network_list = g_variant_new ("(@a(oa{sv}))", returned_service_list);
    }

  return network_list;
}

void
connect_to_network (const gchar *arg_app_name, const gchar *arg_network_id,
                    const gchar *arg_network_type,
                    const gchar *arg_network_passkey)
{
  connect_to_service_object_path (arg_network_id, arg_network_passkey);
}

void
disconnect_to_network (const gchar *arg_app_name, const gchar *arg_network_id,
                       const gchar *arg_network_type)
{
  disconnect_to_service_object_path (arg_network_id);
}

static void
net_connman_service_call_remove_clb (GObject *source_object, GAsyncResult *res,
                                     gpointer user_data)
{
  GError *error = NULL;

  CONNMAN_DNLD_PRINT ("\n Server %s:\n", __FUNCTION__);
  net_connman_service_call_remove_finish ((NetConnmanService *) source_object,
                                          res, &error);
  if (error)
    {
      const gchar *service_path = user_data;
      gchar *striped_error = NULL;
      gchar *dup_error = g_strdup (error->message);

      g_warning ("Could not complete the remove service method %s",
                 error ? error->message : "no error given.");
      striped_error = g_strrstr (dup_error, ":");
      if (striped_error == NULL)
        beckfootconnmancallback.fn_connman_error_cb (service_path,
                                                     error->message);
      else
        {
          beckfootconnmancallback.fn_connman_error_cb (service_path,
                                                       striped_error + 1);
          // dont free the "striped_error" as it was freed by freeing "dup_error"
        }

      g_error_free (error);
      error = NULL;
      g_free (dup_error);
      dup_error = NULL;
    }
}

static void
net_connman_service_remove_proxy_created_clb (GObject *source_object,
                                              GAsyncResult *res,
                                              gpointer user_data)
{
  GError *error = NULL;
  NetConnmanService *net_connman_service_remove_proxy;

  CONNMAN_DNLD_PRINT ("\n Server %s:\n", __FUNCTION__);

  /* finishes the proxy creation and gets the proxy ptr */
  net_connman_service_remove_proxy = net_connman_service_proxy_new_finish (
      res, &error);

  if ((net_connman_service_remove_proxy == NULL) || error)
    {
      g_warning ("Error intializing the net.connman service remove proxy %s",
                 error ? error->message : "no error given.");
    }
  if (NULL != net_connman_service_remove_proxy)
    {
      GList *path_list = NULL;
      service_object_paths = g_list_first (service_object_paths);
      path_list = g_list_find_custom (service_object_paths, user_data,
                                      list_custome_compare);
      if (path_list == NULL)
        {
          g_signal_connect (net_connman_service_remove_proxy,
                            "property-changed",
                            G_CALLBACK (net_connman_services_property_changed),
                            NULL);
          service_object_paths = g_list_append (service_object_paths,
                                                g_strdup ((gchar *) user_data));
        }
      net_connman_service_call_remove (net_connman_service_remove_proxy, NULL,
                                       net_connman_service_call_remove_clb,
                                       user_data);
    }

}

static void
net_connman_service_remove_name_appeared (GDBusConnection *connection,
                                          const gchar *name,
                                          const gchar *name_owner,
                                          gpointer user_data)

{
  CONNMAN_DNLD_PRINT("net.connman service remove Name_appeared \n");
  CONNMAN_DNLD_PRINT(" Service object path = %s\n", (gchar * )user_data);
  net_connman_service_proxy_new (connection, G_DBUS_PROXY_FLAGS_NONE,
                                 "net.connman", (gchar *) user_data, NULL,
                                 net_connman_service_remove_proxy_created_clb,
                                 user_data);

}

static void
net_connman_service_remove_name_vanished (GDBusConnection *connection,
                                          const gchar *name, gpointer user_data)
{
  CONNMAN_DNLD_PRINT("net.connman service remove name_vanished \n");
}

void
remove_service_object_path (const gchar *arg_network_id)
{
  /* Starts watching name on the bus specified by bus_type */
  /* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
  gchar *service_object_path = g_strdup (arg_network_id);
  g_bus_watch_name (G_BUS_TYPE_SYSTEM, "net.connman",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    net_connman_service_remove_name_appeared,
                    net_connman_service_remove_name_vanished,
                    service_object_path, g_free);
}

gchar *
get_objectpath_from_connman (const gchar *mac_address)
{
  GVariant *network_list = NULL;
  GList * service_path_list = NULL;
  gchar *modified_mac_address = NULL, *objpath = NULL;
  gchar *dup_mac_address = g_strdup (mac_address);
  GList *path_list = NULL;

  network_list = net_connection_manager_get_service_method ();
  service_path_list = filter_for_bluetooth_list (network_list);
  modified_mac_address = g_malloc0 (40);
  remove_colon_from_string (dup_mac_address, modified_mac_address);

  path_list = g_list_find_custom (service_path_list, modified_mac_address,
                                  list_custome_search);
  if (path_list != NULL)
    {
      objpath = (gchar *) (path_list->data);
    }

  if (dup_mac_address)
    {
      g_free (dup_mac_address);
      dup_mac_address = NULL;
    }

  if (modified_mac_address)
    {
      g_free (modified_mac_address);
      modified_mac_address = NULL;
    }
  return objpath;
}
