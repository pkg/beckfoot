/* vim:set expandtab ts=4 shiftwidth=4: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * connman_interface.h
 *
 *  Created on: Apr 8, 2013
 */

#ifndef BACKUP_CONNMAN_INTERFACE_H_
#define BACKUP_CONNMAN_INTERFACE_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib.h>

#define BECKFOOT_CONNMAN_AGENT_PATH "/beckfoot/agent/service"
#define NET_CONNMAN_WIFI_TECHNOLOGY_PATH "/net/connman/technology/wifi"
#define NET_CONNMAN_ETHERNET_TECHNOLOGY_PATH "/net/connman/technology/ethernet"
#define NET_CONNMAN_BLUETOOTH_TECHNOLOGY_PATH "/net/connman/technology/bluetooth"

// CONNMAN_SRV_DEBUG
typedef enum
{

  CONNMAN_SRV_DEBUG = 1 << 0,

} ConnmanSrvDebugFlag;
#define CONNMAN_SRV_HAS_DEBUG               ((connman_service_debug_flags) & 1)
#define CONNMAN_DNLD_PRINT( a ...) \
    if (G_LIKELY (CONNMAN_SRV_HAS_DEBUG )) \
    {                                   \
        g_print("CONNMANService::> " a);                \
    }
extern guint connman_service_debug_flags;

typedef struct _TechnologyInfo
{
  gchar *type;
  gboolean powered;
} TechnologyInfo;

void initialise_net_connman_gdbus_proxy (void);

void
filter_for_connected_nw_info (GVariant *network_list);
void
filter_for_technologies_set_accordingly (GVariant * technology_list);

void start_net_connman_agent_service (void);

void
create_proxy_for_connected_service (gchar *service_path);

GVariant *net_connection_manager_get_service_method (void);

void
connect_to_network (const gchar *arg_app_name, const gchar *arg_network_id,
                    const gchar *arg_network_type,
                    const gchar *arg_network_passkey);
void
disconnect_to_network (const gchar *arg_app_name, const gchar *arg_network_id,
                       const gchar *arg_network_type);
void
technology_powered (const gchar *arg_technology_type, gboolean arg_powered);

GVariant *get_network_list (void);

void
remove_service_object_path (const gchar *arg_network_id);
gchar *
get_objectpath_from_connman (const gchar *mac_address);

typedef void
(*connman_error_notify_callback) (const gchar *servicePath,
                                  const gchar *error_message);
typedef void
(*connman_connect_notify_callback) (const gchar *servicePath,
                                    gchar *networkType,
                                    const gchar *arg_connection_status);
typedef void
(*connman_disconnect_notify_callback) (const gchar *servicePath,
                                       gchar *networkType,
                                       const gchar *arg_connection_status);
typedef void
(*connman_technology_notify_callback) (const gchar *technology_type,
                                       gboolean value);
typedef void
(*wifi_connected_network_properties_callback) (const gchar *value,
                                               const gchar *which_property,
                                               guchar signal_strength);
typedef void
(*emit_wifi_signal_strength) (const gchar *object_path, guchar signal_strength);
typedef void
(*bt_connected_network_properties_callback) (const gchar *value,
                                             const gchar *which_property);
typedef void
(*beckfoot_connman_status_notify_callback) (gboolean active);
typedef void
(*set_beckfoot_connman_status_properties) (const gchar *network_id,
                                           const gchar *status,
                                           const gchar *network_type);
void
beckfoot_connection_manager_callback (
    connman_connect_notify_callback fn_connman_connect_cb,
    connman_error_notify_callback fn_connman_error_cb,
    connman_disconnect_notify_callback fn_connman_disconnect_cb,
    connman_technology_notify_callback fn_connman_technology_cb,
    beckfoot_connman_status_notify_callback fn_connman_connection_status_cb,
    set_beckfoot_connman_status_properties fn_connman_set_status_property_cb);
void
beckfoot_wifi_manager_callback (
    wifi_connected_network_properties_callback fn_wifi_connected_nw_cb,
    emit_wifi_signal_strength fn_wifi_signal_strength_cb);
void
beckfoot_bluetooth_manager_properties_callback (
    bt_connected_network_properties_callback fn_connected_nw_notify);

#endif /* BACKUP_CONNMAN_INTERFACE_H_ */
