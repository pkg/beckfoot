/* vim:set expandtab ts=4 shiftwidth=4: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * connection_manager.c
 *
 *  Created on: Apr 4, 2013
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "bluez-interface/bluez_interface.h"
#include "beckfoot.h"
#include "wifi_interface.h"

//#define CONNMAN_PRINT( a ...) g_print(a)
#define CONNMAN_PRINT( a ...)

BeckfootControl *connectionMgrControlProxy;
BeckfootStatus *connectionMgrStatusProxy;
BeckfootWiFi *wifiManagerControlProxy;
BeckfootBluetooth *bluetoothManagerControlProxy;

/**** ConnMan Common API  *******/
static void
beckfoot_network_connect (BeckfootControl *object,
                          GDBusMethodInvocation *invocation,
                          const gchar *arg_app_name,
                          const gchar *arg_network_id,
                          const gchar *arg_network_type,
                          const gchar *arg_network_passkey)
{
  connect_to_network (arg_app_name, arg_network_id, arg_network_type,
                      arg_network_passkey);
  beckfoot_control_complete_network_connect (object, invocation);
}

static void
beckfoot_network_disconnect (BeckfootControl *object,
                             GDBusMethodInvocation *invocation,
                             const gchar *arg_app_name,
                             const gchar *arg_network_id,
                             const gchar *arg_network_type)
{
  disconnect_to_network (arg_app_name, arg_network_id, arg_network_type);
  beckfoot_control_complete_network_disconnect (object, invocation);
}

static void
beckfoot_technology_powered (BeckfootControl *object,
                             GDBusMethodInvocation *invocation,
                             const gchar *arg_technology_type,
                             gboolean arg_powered)
{
  technology_powered (arg_technology_type, arg_powered);
  beckfoot_control_complete_technology_powered (object, invocation);
}
/**** End *******/

/**** WIFI *******/
static void
beckfoot_get_network_list (BeckfootWiFi *object,
                           GDBusMethodInvocation *invocation)
{
  GVariant *ret = NULL;
  ret = get_wifi_network_list ();
  if (NULL != ret)
    beckfoot_wi_fi_complete_get_network_list (object, invocation, ret);
}

static void
beckfoot_network_remove (BeckfootWiFi *object,
                         GDBusMethodInvocation *invocation,
                         const gchar *arg_network_id)
{
  remove_from_wifi_network (arg_network_id);
  beckfoot_wi_fi_complete_network_remove (object, invocation);
}
/**** End *******/

/******* Bluetooth *********/

static void
beckfoot_get_object_path (BeckfootBluetooth *object,
                          GDBusMethodInvocation *invocation,
                          const gchar *arg_address)
{
  const gchar *objectpath = get_objectpath_from_connman (arg_address);
  beckfoot_bluetooth_complete_get_object_path (object, invocation, objectpath);
}

/**** End *******/

static void
connection_manager_cleanup (void)
{
  CONNMAN_PRINT("%s: Called\n", __FUNCTION__);

  /*Deinitialise all resources allocated for the Connection Manager */
}

static void
termination_handler (int signum)
{
  CONNMAN_PRINT("%s: Called\n", __FUNCTION__);
  exit (0);
}

static void
register_exit_functionalities (void)
{
  struct sigaction action;

  CONNMAN_PRINT ("%s: Called\n", __FUNCTION__);

  action.sa_handler = termination_handler;
  action.sa_flags = 0;
  sigaction (SIGINT, &action, NULL);
  sigaction (SIGHUP, &action, NULL);
  sigaction (SIGTERM, &action, NULL);
  atexit (connection_manager_cleanup);
}

static void
connman_connect_notify_cb (const gchar *servicePath, gchar *networkType,
                           const gchar *arg_connection_status)
{
  CONNMAN_PRINT("ConnmanServer ::> connect status --> %s\n", arg_connection_status); CONNMAN_PRINT("ConnmanServer ::> connected service path --> %s\n", servicePath);

  if (NULL != connectionMgrControlProxy)
    {
      beckfoot_control_emit_network_connected (connectionMgrControlProxy,
                                               servicePath, networkType,
                                               arg_connection_status);
      if (g_strrstr (servicePath, "bluetooth"))
        {
          // set BT connected n/w properties
        }
      else if (g_strrstr (servicePath, "wifi"))
        {
          // set wifi connected n/w properties
        }
      else if (g_strrstr (servicePath, "ethernet"))
        {
          // set ethernet connected n/w properties
        }
      else if (g_strrstr (servicePath, "3g"))
        {
          // set 3g connected n/w properties
        }

#if 0
      if( (g_strcmp0(arg_connection_status, "ready") == 0) || (g_strcmp0(arg_connection_status, "online") == 0))
        {
          beckfoot_control_set_connected_network_status(self.connectionMgrControl, arg_connection_status);
          beckfoot_control_set_connected_network_id(self.connectionMgrControl, servicePath);
          initialise_controll_properties("wifi", "");
        }
      else if( (g_strcmp0(arg_connection_status, "idle") == 0))
        {
          beckfoot_control_set_connected_network_status(self.connectionMgrControl, arg_connection_status);
          beckfoot_control_set_connected_network_id(self.connectionMgrControl, "");
          //beckfoot_control_set_connected_network_strength(self.connectionMgrControl, "");
        }
#endif
    }
}

static void
connman_disconnect_notify_cb (const gchar *servicePath, gchar *networkType,
                              const gchar *arg_connection_status)
{
  CONNMAN_PRINT("ConnmanServer ::> disconnect status --> %s\n", arg_connection_status); CONNMAN_PRINT("ConnmanServer ::> disconnected service path --> %s\n", servicePath);
  if (NULL != connectionMgrControlProxy)
    {
      beckfoot_control_emit_network_disconnected (connectionMgrControlProxy,
                                                  servicePath, networkType,
                                                  arg_connection_status);
      if (g_strrstr (servicePath, "bluetooth"))
        {
          // set BT connected n/w properties
        }
      else if (g_strrstr (servicePath, "wifi"))
        {
          // set wifi connected n/w properties
        }
      else if (g_strrstr (servicePath, "ethernet"))
        {
          // set ethernet connected n/w properties
        }
      else if (g_strrstr (servicePath, "3g"))
        {
          // set 3g connected n/w properties
        }
#if 0
      beckfoot_control_set_connected_network_status(self.connectionMgrControl, "idle");
      beckfoot_control_set_connected_network_id(self.connectionMgrControl, "");
      //beckfoot_control_set_connected_network_strength(self.connectionMgrControl, "");
#endif
    }
}

static void
connman_error_notify_cb (const gchar *servicePath, const gchar *error_message)
{
  CONNMAN_PRINT("ConnmanServer ::> Error is --> %s\n", error_message); CONNMAN_PRINT("ConnmanServer ::> Error occurred on service path --> %s\n", servicePath);
  if (NULL != connectionMgrControlProxy)
    beckfoot_control_emit_connection_error (connectionMgrControlProxy,
                                            servicePath, error_message);
}

static void
set_technologies_controll_properties (const gchar *technology_type,
                                      gboolean value)
{
  if (NULL != connectionMgrControlProxy)
    {
      if (g_strcmp0 (technology_type, "wifi") == 0)
        {
          CONNMAN_PRINT("ConnmanServer ::> setting Wifi powered--> %d\n", value);
          beckfoot_control_set_wi_fi_powered_status (connectionMgrControlProxy,
                                                     value);
          g_object_notify ((GObject *) connectionMgrControlProxy,
                           "wi-fi-powered-status");
        }
      else if (g_strcmp0 (technology_type, "ethernet") == 0)
        {
          CONNMAN_PRINT("ConnmanServer ::> setting ethernet powered--> %d\n", value);

        }
      else if (g_strcmp0 (technology_type, "bluetooth") == 0)
        {
          CONNMAN_PRINT("ConnmanServer ::> setting bluetooth powered--> %d\n", value);
          beckfoot_control_set_bluetooth_powered_status (
              connectionMgrControlProxy, value);
        }
      else if (g_strcmp0 (technology_type, "3g") == 0)
        {
          CONNMAN_PRINT("ConnmanServer ::> setting 3g powered--> %d\n", value);
          beckfoot_control_set_three_gpowered_status (connectionMgrControlProxy,
                                                      value);
        }
    }

}

static void
on_beckfoot_conn_man_bus_acquired (GDBusConnection *connection,
                                   const gchar *name, gpointer user_data)
{
  CONNMAN_PRINT("Server %s:\n", __FUNCTION__);
  /* Creates a new skeleton object, ready to be exported */
  connectionMgrControlProxy = beckfoot_control_skeleton_new ();
  connectionMgrStatusProxy = beckfoot_status_skeleton_new ();
  wifiManagerControlProxy = beckfoot_wi_fi_skeleton_new ();
  bluetoothManagerControlProxy = beckfoot_bluetooth_skeleton_new ();

  if (NULL != connectionMgrControlProxy)
    {
      /* provide signal handlers for all methods */

      g_signal_connect (connectionMgrControlProxy, "handle_network_connect",
                        G_CALLBACK (beckfoot_network_connect), NULL);
      g_signal_connect (connectionMgrControlProxy, "handle_network_disconnect",
                        G_CALLBACK (beckfoot_network_disconnect), NULL);
      g_signal_connect (connectionMgrControlProxy, "handle_technology_powered",
                        G_CALLBACK (beckfoot_technology_powered), NULL);
      //connection_manager_register_control_handlers(self.connectionMgrControl);

      /* Exports interface connectionMgrControl at object_path "/Beckfoot/ConnectionManager/Control" on connection */
      if (!g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (connectionMgrControlProxy), connection,
          "/org/apertis/Beckfoot/Control", NULL))
        {
          CONNMAN_PRINT ("%s: Control interface export error\n", __FILE__);
        }

    }

  if (NULL != connectionMgrStatusProxy)
    {
      /* provide signal handlers for all methods */
      //connection_manager_register_status_handlers(self.connectionMgrStatus);
      beckfoot_status_set_connection_status (connectionMgrStatusProxy,
                                             BECKFOOT_NETWORK_STATE_UNKNOWN);
      beckfoot_status_set_active_network_type (connectionMgrStatusProxy,
                                               BECKFOOT_NETWORK_TYPE_UNKNOWN);
      beckfoot_status_set_active_network_name (connectionMgrStatusProxy,
                                               BECKFOOT_NETWORK_NAME_UNKNOWN);
      beckfoot_status_set_wi_fi_active_network_strength (
          connectionMgrStatusProxy, 0);
      beckfoot_status_set_is_connected (connectionMgrStatusProxy, 0);
      /* Exports interface connectionMgrStatus at object_path "/Beckfoot/ConnectionManager/Status" on connection */
      if (!g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (connectionMgrStatusProxy), connection,
          "/org/apertis/Beckfoot/Status", NULL))
        {
          CONNMAN_PRINT ("%s: Status Interface export error \n", __FILE__);
        }
    }

  if (NULL != wifiManagerControlProxy)
    {
      g_signal_connect (wifiManagerControlProxy, "handle_get_network_list",
                        G_CALLBACK (beckfoot_get_network_list), NULL);
      g_signal_connect (wifiManagerControlProxy, "handle_network_remove",
                        G_CALLBACK (beckfoot_network_remove), NULL);

      if (!g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (wifiManagerControlProxy), connection,
          "/org/apertis/Beckfoot/WiFi", NULL))
        {
          CONNMAN_PRINT ("%s: WiFi control Interface export error \n", __FILE__);
        }

    }

  if (NULL != bluetoothManagerControlProxy)
    {
      g_signal_connect (bluetoothManagerControlProxy, "handle_get_object_path",
                        G_CALLBACK (beckfoot_get_object_path), NULL);

      if (!g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (bluetoothManagerControlProxy), connection,
          "/org/apertis/Beckfoot/Bluetooth", NULL))
        {
          CONNMAN_PRINT ("%s: Bluetooth control Interface export error \n", __FILE__);
        }
    }
}

static void
on_beckfoot_conn_man_name_acquired (GDBusConnection *connection,
                                    const gchar *name, gpointer user_data)
{
  /* Nothing to do */
}

static void
on_beckfoot_conn_man_name_lost (GDBusConnection *connection, const gchar *name,
                                gpointer user_data)
{
  /* free all dbus handlers ... */
}

static void
initialise_beckfoot_services (void)
{
  g_bus_own_name (G_BUS_TYPE_SESSION,  // bus type
      "org.apertis.Beckfoot",  // interface name
      G_BUS_NAME_OWNER_FLAGS_NONE,  // bus own flag, can be used to take away the bus and give it to another service
      on_beckfoot_conn_man_bus_acquired,  // callback invoked when the bus is acquired
      on_beckfoot_conn_man_name_acquired,  // callback invoked when interface name is acquired
      on_beckfoot_conn_man_name_lost,  // callback invoked when name is lost to another service or other reason
      NULL,  // user data
      NULL  // user data free func
      );
}

static void
set_wifi_connected_network_properties (const gchar *value,
                                       const gchar *which_property,
                                       guchar signal_strength)
{
  CONNMAN_PRINT("ConnmanServer ::> Function %s\n", __FUNCTION__); CONNMAN_PRINT("ConnmanServer ::> Value is %s\n", value);

  if (g_strcmp0 (which_property, "Status") == 0)
    beckfoot_wi_fi_set_connected_network_status (wifiManagerControlProxy,
                                                 value);
  else if (g_strcmp0 (which_property, "ObjectPath") == 0)
    beckfoot_wi_fi_set_connected_network_id (wifiManagerControlProxy, value);
  else if (g_strcmp0 (which_property, "SignalStrength") == 0)
    {
      if ((g_strcmp0 (
          value,
          beckfoot_wi_fi_get_connected_network_id (wifiManagerControlProxy))
          == 0))
        {
          beckfoot_status_set_wi_fi_active_network_strength (
              connectionMgrStatusProxy, signal_strength);
          beckfoot_wi_fi_set_connected_network_strength (
              wifiManagerControlProxy, signal_strength);
          if (g_strcmp0 (
              "Online",
              beckfoot_wi_fi_get_connected_network_status (
                  wifiManagerControlProxy)) == 0)
            beckfoot_status_emit_signal_strength_changed (
                connectionMgrStatusProxy, value, signal_strength);
          beckfoot_wi_fi_emit_signal_strength_changed (wifiManagerControlProxy,
                                                       value, signal_strength);
        }

    }
}

static void
emit_wifi_signal_strength_changed (const gchar *object_path,
                                   guchar signal_strength)
{
  CONNMAN_PRINT("ConnmanServer ::> Function %s\n", __FUNCTION__); CONNMAN_PRINT("ConnmanServer ::> Signal strength is %d\n", signal_strength);

  if ((g_strcmp0 (
      object_path,
      beckfoot_wi_fi_get_connected_network_id (wifiManagerControlProxy)) == 0))
    {
      beckfoot_status_set_wi_fi_active_network_strength (
          connectionMgrStatusProxy, signal_strength);
      beckfoot_wi_fi_set_connected_network_strength (wifiManagerControlProxy,
                                                     signal_strength);
      beckfoot_wi_fi_emit_signal_strength_changed (wifiManagerControlProxy,
                                                   object_path,
                                                   signal_strength);

      if (g_strcmp0 (
          beckfoot_wi_fi_get_connected_network_status (wifiManagerControlProxy),
          BECKFOOT_NETWORK_STATE_ONLINE) == 0)
        beckfoot_status_emit_signal_strength_changed (connectionMgrStatusProxy,
                                                      object_path,
                                                      signal_strength);
    }

}

static void
emit_beckfoot_connman_status_signal (gboolean active)
{
  const gchar *network_id = beckfoot_status_get_active_network_name (
      connectionMgrStatusProxy);
  const gchar *network_type = beckfoot_status_get_active_network_type (
      connectionMgrStatusProxy);

  CONNMAN_PRINT ("ConnmanServer ::> Function %s\n", __FUNCTION__);
  CONNMAN_PRINT ("ConnmanServer ::> State is %d\n", active);

  if (active)
    {
      if ((network_type != NULL) && (network_id != NULL))
        {
          CONNMAN_PRINT("ConnmanServer ::> Network Type = %s  and Network Id = %s \n", network_type, network_id);
          beckfoot_status_set_is_connected (connectionMgrStatusProxy, TRUE);
          beckfoot_status_emit_connection_active (connectionMgrStatusProxy,
                                                  network_id, network_type);
          //  g_object_notify((GObject *)connectionMgrStatusProxy, "is-connected");
        }
    }
  else
    {
      beckfoot_status_set_is_connected (connectionMgrStatusProxy, FALSE);
      beckfoot_status_emit_active_connection_lost (connectionMgrStatusProxy, "",
                                                   "");
      //	g_object_notify((GObject *)connectionMgrStatusProxy, "is-connected");
    }
}

static void
set_beckfoot_connman_status_property (const gchar *network_id,
                                      const gchar *status,
                                      const gchar *network_type)
{
  CONNMAN_PRINT("ConnmanServer ::> Function %s\n", __FUNCTION__);

  if (network_id != NULL)
    {
      CONNMAN_PRINT("ConnmanServer ::> Setting network_id property is %s\n", network_id);
      beckfoot_status_set_active_network_name (connectionMgrStatusProxy,
                                               network_id);
      g_object_notify ((GObject *) connectionMgrStatusProxy,
                       "active-network-name");
    }
  if (status != NULL)
    {
      CONNMAN_PRINT("ConnmanServer ::> Setting status property is %s\n", status);
      beckfoot_status_set_connection_status (connectionMgrStatusProxy, status);
      g_object_notify ((GObject *) connectionMgrStatusProxy,
                       "connection-status");
    }
  if (network_type != NULL)
    {
      CONNMAN_PRINT("ConnmanServer ::> Setting network Type property is %s\n", network_type);
      beckfoot_status_set_active_network_type (connectionMgrStatusProxy,
                                               network_type);
      g_object_notify ((GObject *) connectionMgrStatusProxy,
                       "active-network-type");
    }
}

static void
set_Bt_connected_network_properties (const gchar *value,
                                     const gchar *which_property)
{
  CONNMAN_PRINT("ConnmanServer ::> Function %s\n", __FUNCTION__); CONNMAN_PRINT("ConnmanServer ::> Value is %s\n", value);

  if (g_strcmp0 (which_property, "Status") == 0)
    beckfoot_bluetooth_set_connected_network_status (
        bluetoothManagerControlProxy, value);
  else if (g_strcmp0 (which_property, "ObjectPath") == 0)
    beckfoot_bluetooth_set_connected_network_id (bluetoothManagerControlProxy,
                                                 value);
}

gint
main (gint argc, gchar *argv[])
{
  GMainLoop *mainloop;

  CONNMAN_PRINT ("Launched: connection_manager\n");

  mainloop = g_main_loop_new (NULL, FALSE);
  register_exit_functionalities ();
  beckfoot_connection_manager_callback (connman_connect_notify_cb,
                                        connman_error_notify_cb,
                                        connman_disconnect_notify_cb,
                                        set_technologies_controll_properties,
                                        emit_beckfoot_connman_status_signal,
                                        set_beckfoot_connman_status_property);

  beckfoot_bluetooth_manager_properties_callback (
      set_Bt_connected_network_properties);
  beckfoot_wifi_manager_callback (set_wifi_connected_network_properties,
                                  emit_wifi_signal_strength_changed);

  initialise_beckfoot_services ();
  initialise_net_connman_gdbus_proxy ();

  CONNMAN_PRINT("Running mainloop ...\n");
  g_main_loop_run (mainloop);

  CONNMAN_PRINT("Exiting: connection_manager...\n");
  exit (0);
}
